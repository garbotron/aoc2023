/*
Another part 2 switchemup.
A simple scan shows that there are only 34 intersection points where you can go multiple directions.
So the first step is to collapse the graph into sections with links to other sections.
*/

use std::{collections::{HashMap, HashSet}, fs};

enum Cell {
    Open,
    Wall,
    Slope { dx: i64, dy: i64 },
}

struct Map {
    cells: HashMap<(i64, i64), Cell>,
    start: (i64, i64),
    goal: (i64, i64),
}

struct IntersectionGraph {
    connections: HashMap<usize, HashSet<(usize, i64)>>,
}

impl Map {
    fn is_available(&self, cell: (i64, i64)) -> bool {
        match self.cells.get(&cell) {
            Some(Cell::Open) | Some(Cell::Slope { .. }) => true,
            _ => false,
        }
    }
}

fn parse_cell(c: char) -> Cell {
    match c {
        '.' => Cell::Open,
        '#' => Cell::Wall,
        '<' => Cell::Slope { dx: -1, dy: 0 },
        '>' => Cell::Slope { dx: 1, dy: 0 },
        '^' => Cell::Slope { dx: 0, dy: -1 },
        'v' => Cell::Slope { dx: 0, dy: 1 },
        _ => panic!(),
    }
}

fn parse_map(s: &str) -> Map {
    let mut cells = HashMap::new();
    let mut start = (i64::MAX, i64::MAX);
    let mut goal = (0, 0);
    for (y, line) in s.lines().enumerate() {
        let y = y as i64;
        for (x, ch) in line.chars().enumerate() {
            let x = x as i64;
            let cell = parse_cell(ch);
            if matches!(cell, Cell::Open) {
                if y < start.1 {
                    start = (x, y);
                }
                if y > goal.1 {
                    goal = (x, y);
                }
            }
            cells.insert((x, y), cell);
        }
    }
    Map { cells, start, goal }
}

fn next_steps(map: &Map, (x, y): (i64, i64), prev: (i64, i64)) -> Vec<(i64, i64)> {
    let neighbors = match map.cells.get(&(x, y)) {
        Some(Cell::Open) => vec![(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)],
        Some(Cell::Slope { dx, dy }) => vec![(x + dx, y + dy)],
        _ => panic!(),
    };
    neighbors.iter().copied().filter(|x| map.is_available(*x) && *x != prev).collect()
}

fn create_intersection_graph(map: &Map) -> IntersectionGraph {
    let mut intersections = HashMap::from([(map.start, 0), (map.goal, 1)]);
    let mut last_intersection_id = 1;
    let mut connections: HashMap<usize, HashSet<(usize, i64)>> = HashMap::new();
    let mut seen = HashSet::new();

    let mut to_explore = vec![(map.start, (map.start.0, map.start.1 + 1), 0, 1)];
    while let Some((prev, cur, last_intersection, steps)) = to_explore.pop() {
        if !seen.insert((prev, cur)) {
            continue;
        }

        let next_steps = next_steps(map, cur, prev);
        if cur == map.goal {
            connections.entry(last_intersection).or_default().insert((1, steps));
        } else if next_steps.is_empty() {
            // do nothing
        } else if next_steps.len() > 1 {
            let idx = *intersections.entry(cur).or_insert_with(|| { last_intersection_id += 1; last_intersection_id });
            connections.entry(last_intersection).or_default().insert((idx, steps));
            for n in next_steps {
                to_explore.push((cur, n, idx, 1));
            }
        } else {
            to_explore.push((cur, next_steps[0], last_intersection, steps + 1));
        }
    }

    IntersectionGraph { connections }
}

fn longest_path(graph: &IntersectionGraph) -> i64 {
    let mut cache = HashMap::new();
    let mut to_explore = vec![(0usize, 0u64, 0i64)];
    let mut best = 0;

    while let Some((intersection, seen_bmp, len)) = to_explore.pop() {
        if intersection == 1 {
            best = best.max(len);
            continue;
        }

        if let Some(prev) = cache.get(&(intersection, seen_bmp)) {
            if *prev >= len {
                continue;
            }
        }

        cache.insert((intersection, seen_bmp), len);
        for (next, dist) in graph.connections.get(&intersection).unwrap_or(&HashSet::new()).iter() {
            if seen_bmp & (1 << *next) == 0 {
                to_explore.push((*next, seen_bmp | (1 << intersection), len + dist));
            }
        }
    }

    best
}

fn main() {
    let input = fs::read_to_string("input/day-23.txt").unwrap();
    let mut map = parse_map(&input);

    println!("Part 1: {}", longest_path(&create_intersection_graph(&map)));

    for c in map.cells.values_mut() {
        if matches!(c, Cell::Slope { .. }) {
            *c = Cell::Open;
        }
    }

    println!("Part 2: {}", longest_path(&create_intersection_graph(&map)));
}
