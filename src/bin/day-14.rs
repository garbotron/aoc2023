use std::{fs, collections::{BTreeSet, HashMap}};
use itertools::Itertools;


#[derive(Clone, Debug, PartialEq, Eq, Hash)]
struct Map {
    width: i32,
    height: i32,
    rocks: BTreeSet<(i32, i32)>,
    barriers: BTreeSet<(i32, i32)>,
}

fn parse_map(s: &str) -> Map {
    let lines = s.lines().collect_vec();
    let mut ret = Map { width: lines[0].len() as i32, height: lines.len() as i32, rocks: BTreeSet::new(), barriers: BTreeSet::new() };
    for (y, l) in lines.iter().enumerate() {
        for (x, ch) in l.chars().enumerate() {
            match ch {
                '#' => { ret.barriers.insert((x as i32, y as i32)); },
                'O' => { ret.rocks.insert((x as i32, y as i32)); },
                '.' => {},
                _ => panic!(),
            }
        }
    }
    ret
}

fn tilt_north(m: &mut Map) {
    for x in 0..m.width {
        for y in 1..m.height {
            if m.rocks.contains(&(x, y)) {
                let mut target_y = y;
                while target_y > 0 && !(m.rocks.contains(&(x, target_y - 1)) || m.barriers.contains(&(x, target_y - 1))) {
                    target_y -= 1;
                }
                if target_y != y {
                    m.rocks.remove(&(x, y));
                    m.rocks.insert((x, target_y));
                }
            }
        }
    }
}

fn tilt_south(m: &mut Map) {
    for x in 0..m.width {
        for y in (0..(m.height-1)).rev() {
            if m.rocks.contains(&(x, y)) {
                let mut target_y = y;
                while target_y < (m.height - 1) && !(m.rocks.contains(&(x, target_y + 1)) || m.barriers.contains(&(x, target_y + 1))) {
                    target_y += 1;
                }
                if target_y != y {
                    m.rocks.remove(&(x, y));
                    m.rocks.insert((x, target_y));
                }
            }
        }
    }
}

fn tilt_west(m: &mut Map) {
    for y in 0..m.height {
        for x in 1..m.width {
            if m.rocks.contains(&(x, y)) {
                let mut target_x = x;
                while target_x > 0 && !(m.rocks.contains(&(target_x - 1, y)) || m.barriers.contains(&(target_x - 1, y))) {
                    target_x -= 1;
                }
                if target_x != x {
                    m.rocks.remove(&(x, y));
                    m.rocks.insert((target_x, y));
                }
            }
        }
    }
}

fn tilt_east(m: &mut Map) {
    for y in 0..m.height {
        for x in (0..(m.width-1)).rev() {
            if m.rocks.contains(&(x, y)) {
                let mut target_x = x;
                while target_x < (m.width - 1) && !(m.rocks.contains(&(target_x + 1, y)) || m.barriers.contains(&(target_x + 1, y))) {
                    target_x += 1;
                }
                if target_x != x {
                    m.rocks.remove(&(x, y));
                    m.rocks.insert((target_x, y));
                }
            }
        }
    }
}

fn tilt_cycle(m: &mut Map) {
    tilt_north(m);
    tilt_west(m);
    tilt_south(m);
    tilt_east(m);
}

fn cycle_until_stable(m: &mut Map) -> (u64, u64) {
    let mut cache = HashMap::new();
    for cycle_count in 0.. {
        if let Some(prev) = cache.get(m) {
            return (*prev, cycle_count - prev);
        }
        cache.insert(m.clone(), cycle_count);
        tilt_cycle(m);
    }
    panic!()
}

fn score(m: &Map) -> i32 {
    let mut ret = 0;
    for (_, y) in m.rocks.iter() {
        ret += m.height - *y;
    }
    ret
}

fn main() {
    let input = fs::read_to_string("input/day-14.txt").unwrap();
    let mut map = parse_map(&input);
    tilt_north(&mut map);

    println!("Part 1: {}", score(&map));

    let (cycle_start, cycle_len) = cycle_until_stable(&mut map);
    let remaining_cycles = (1000000000 - cycle_start) % cycle_len;
    for _ in 0..remaining_cycles {
        tilt_cycle(&mut map);
    }
    println!("Part 2: {}", score(&map));
}
