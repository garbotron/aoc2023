// I don't feel the need to implement A* manually in Rust yet again, so I'll just use the pathfinding crate.

use std::fs;
use itertools::Itertools;

#[derive(PartialEq, Eq, Clone, Hash, Debug)]
struct State { x: i32, y: i32, dx: i32, dy: i32, cur_run: i32 }

fn successors(s: &State, map: &Vec<Vec<i32>>, min_run: i32, max_run: i32) -> Vec<(State, i32)> {
    let mut next = Vec::new();

    if s.cur_run < max_run {
        next.push(State { x: s.x + s.dx, y: s.y + s.dy, cur_run: s.cur_run + 1, ..*s });
    }

    if s.cur_run >= min_run || (s.x == 0 && s.y == 0) {
        next.push(State { x: s.x + s.dy, y: s.y + s.dx, dx: s.dy, dy: s.dx, cur_run: 1 });
        next.push(State { x: s.x - s.dy, y: s.y - s.dx, dx: -s.dy, dy: -s.dx, cur_run: 1 });
    }

    next.iter()
        .filter(|s| s.x >= 0 && s.y >= 0 && s.x < map[0].len() as i32 && s.y < map.len() as i32)
        .map(|s| (s.clone(), map[s.y as usize][s.x as usize]))
        .collect()
}

fn main() {
    let input = fs::read_to_string("input/day-17.txt").unwrap();
    let map = input.lines().map(|x| x.chars().map(|y| y.to_string().parse().unwrap()).collect_vec()).collect_vec();

    let result_p1 = pathfinding::directed::astar::astar(
        &State { x: 0, y: 0, dx: 1, dy: 0, cur_run: 0 },
        |s| successors(s, &map, 1, 3),
        |s| -(s.x + s.y),
        |s| s.x == map[0].len() as i32 - 1 && s.y == map.len() as i32 - 1);

    println!("Part 1: {}", result_p1.unwrap().1);

    let result_p2 = pathfinding::directed::astar::astar(
        &State { x: 0, y: 0, dx: 1, dy: 0, cur_run: 0 },
        |s| successors(s, &map, 4, 10),
        |s| -(s.x + s.y),
        |s| s.x == map[0].len() as i32 - 1 && s.y == map.len() as i32 - 1 && s.cur_run >= 4);

    println!("Part 2: {}", result_p2.unwrap().1);
}
