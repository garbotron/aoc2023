use std::{fs, collections::HashMap};
use itertools::Itertools;

#[derive(Debug)]
enum Dest<'a> {
    Accept,
    Reject,
    SendTo(&'a str),
}

#[derive(Debug)]
enum Oper {
    LessThan,
    GreaterThan,
}

#[derive(Debug)]
struct Rule<'a> {
    attr: char,
    op: Oper,
    num: i64,
    dest: Dest<'a>,
}

#[derive(Debug)]
struct Workflow<'a> {
    rules: Vec<Rule<'a>>,
    default: Dest<'a>,
}

type Part = HashMap<char, i64>;
type PartRange = HashMap<char, (i64, i64)>;

fn parse_dest(s: &str) -> Dest {
    match s {
        "A" => Dest::Accept,
        "R" => Dest::Reject,
        x => Dest::SendTo(x),
    }
}

fn parse_rule(s: &str) -> Rule {
    let (a, b) = s.split_once(':').unwrap();
    let dest = parse_dest(b);
    if let Some((x, y)) = a.split_once('<') {
        Rule { attr: x.chars().next().unwrap(), dest, op: Oper::LessThan, num: y.parse().unwrap() }
    } else if let Some((x, y)) = a.split_once('>') {
        Rule { attr: x.chars().next().unwrap(), dest, op: Oper::GreaterThan, num: y.parse().unwrap() }
    } else {
        panic!()
    }
}

fn parse_workflow(s: &str) -> (&str, Workflow) {
    let (name, lst) = s.trim_end_matches('}').split_once('{').unwrap();
    let rules = lst.split(',').collect_vec();
    (name, Workflow {
        rules: rules[..rules.len() - 1].iter().map(|x| parse_rule(x)).collect_vec(),
        default: parse_dest(&rules[rules.len() - 1]),
    })
}

fn parse_part(s: &str) -> Part {
    let mut ret = HashMap::new();
    for s in s.trim_matches('{').trim_matches('}').split(',') {
        let (a, b) = s.split_once('=').unwrap();
        ret.insert(a.chars().next().unwrap(), b.parse().unwrap());
    }
    ret
}

fn satisfies(part: &Part, rule: &Rule) -> bool {
    let a = *part.get(&rule.attr).unwrap();
    match rule.op {
        Oper::LessThan => a < rule.num,
        Oper::GreaterThan => a > rule.num,
    }
}

fn run<'a>(part: &Part, workflow: &str, workflows: &'a HashMap<&str, Workflow<'a>>) -> &'a Dest<'a> {
    let workflow = workflows.get(workflow).unwrap();
    let dest = if let Some(rule) = workflow.rules.iter().find(|r| satisfies(part, r)) {
        &rule.dest
    } else {
        &workflow.default
    };
    match dest {
        Dest::SendTo(x) => run(part, x, workflows),
        x => x,
    }
}

fn find_acceptable_range_count_for_dest(range: &PartRange, dest: &Dest, workflows: &HashMap<&str, Workflow>) -> i64 {
    match dest {
        Dest::Accept => range.values().map(|(start, end)| *end - *start + 1).fold(1, |acc, x| acc * x),
        Dest::Reject => 0,
        Dest::SendTo(x) => find_acceptable_range_count(range, x, workflows),
    }
}

fn find_acceptable_range_count(range: &PartRange, workflow: &str, workflows: &HashMap<&str, Workflow>) -> i64 {
    let workflow = workflows.get(workflow).unwrap();
    let mut sum = 0;
    let mut range = range.clone();
    let mut any_left = true;
    for rule in workflow.rules.iter() {
        let &(cur_start, cur_end) = range.get(&rule.attr).unwrap();

        let (rule_match, rem_match) = match rule.op {
            Oper::LessThan if cur_start >= rule.num => (None, Some((cur_start, cur_end))),
            Oper::GreaterThan if cur_end <= rule.num => (None, Some((cur_start, cur_end))),
            Oper::LessThan if cur_end < rule.num => (Some((cur_start, cur_end)), None),
            Oper::GreaterThan if cur_start > rule.num => (Some((cur_start, cur_end)), None),
            Oper::LessThan => (Some((cur_start, rule.num - 1)), Some((rule.num, cur_end))),
            Oper::GreaterThan => (Some((rule.num + 1, cur_end)), Some((cur_start, rule.num))),
        };

        if let Some(r) = rule_match {
            range.insert(rule.attr, r);
            sum += find_acceptable_range_count_for_dest(&range, &rule.dest, workflows);
        }

        if let Some(r) = rem_match {
            range.insert(rule.attr, r);
        } else {
            any_left = false;
            break;
        }
    }
    if any_left {
        sum += find_acceptable_range_count_for_dest(&range, &workflow.default, workflows);
    }
    sum
}

fn main() {
    let input = fs::read_to_string("input/day-19.txt").unwrap();
    let (rules_block, parts_block) = input.split_once("\n\n").unwrap();
    let workflows: HashMap<_, _> = rules_block.lines().map(parse_workflow).collect();
    let parts = parts_block.lines().map(parse_part).collect_vec();

    let p1_total = parts.iter().filter(|p| matches!(run(p, "in", &workflows), Dest::Accept)).flat_map(|p| p.values()).sum::<i64>();
    println!("Part 1: {p1_total}");

    let starting_ranges: HashMap<_, _> = ['x', 'm', 'a', 's'].iter().map(|c| (*c, (1, 4000))).collect();
    println!("Part 2: {}", find_acceptable_range_count(&starting_ranges, "in", &workflows));
}
