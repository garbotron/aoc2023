use std::{fs, collections::HashMap};

use itertools::Itertools;

fn main() {
    let input = fs::read_to_string("input/day-10.txt").unwrap();
    let lines = input.lines().collect_vec();

    let mut char_map = HashMap::new();
    for (y, line) in lines.iter().enumerate() {
        for (x, ch) in line.chars().enumerate() {
            char_map.insert((x, y), ch);
        }
    }

    let (&start, _) = char_map.iter().find(|(_, ch)| **ch == 'S').unwrap();
    let mut edges: HashMap<(usize, usize), Vec<(usize, usize)>> = HashMap::new();

    // Add all up/down connections
    for y in 0..(lines.len() - 1) {
        for x in 0..lines[0].len() {
            let t = char_map.get(&(x, y)).unwrap();
            let b = char_map.get(&(x, y + 1)).unwrap();
            if ['|', '7', 'F', 'S'].contains(t) && ['|', 'L', 'J', 'S'].contains(b) {
                edges.entry((x, y)).or_default().push((x, y + 1));
                edges.entry((x, y + 1)).or_default().push((x, y));
            }
        }
    }

    // Add all left/right connections
    for y in 0..lines.len() {
        for x in 0..(lines[0].len() - 1) {
            let l = char_map.get(&(x, y)).unwrap();
            let r = char_map.get(&(x + 1, y)).unwrap();
            if ['-', 'L', 'F', 'S'].contains(l) && ['-', 'J', '7', 'S'].contains(r) {
                edges.entry((x, y)).or_default().push((x + 1, y));
                edges.entry((x + 1, y)).or_default().push((x, y));
            }
        }
    }

    // Part 1: Dijkstra's
    let mut costs = HashMap::new();
    let mut to_search = vec![(start, 0)];
    while let Some((node, cost)) = to_search.pop() {
        if cost < *costs.get(&node).unwrap_or(&i32::MAX) {
            costs.insert(node, cost);
            for &neighbor in edges.get(&node).unwrap_or(&vec![]) {
                to_search.push((neighbor, cost + 1));
            }
        }
    }
    println!("Part 1: {}", costs.values().max().unwrap());

    // Part 2: Determine cells that are inside the shape using the old "draw a line and see how many times it intersects" thing.
    // The trick here is that we're dealing with cells, not just points.
    // For the purpose of drawing the line, we'll say the L/R connection point is always at the bottom of the cell, and the line is being drawn through the middle of the cell.
    // Thus we won't intersect with '-', '7' or 'F'.
    let mut enclosed_tile_count = 0;
    for y in 0..lines.len() {
        let mut inside = false;
        for x in 0..lines[0].len() {
            if costs.contains_key(&(x, y)) {
                if !['-', '7', 'F'].contains(char_map.get(&(x, y)).unwrap()) {
                    inside = !inside;
                }
            } else {
                if inside {
                    enclosed_tile_count += 1;
                }
            }
        }
    }
    println!("Part 2: {enclosed_tile_count}");
}
