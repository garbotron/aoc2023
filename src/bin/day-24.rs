/*

Let's look at part 2 again from scratch...

Starting position: S (Sx, Sy, Sz)
Starting velocity: V (Vx, Vy, Vz)

Hailstone # position: H# (H1x, H1y, H1z)
Hailstone # velocity: T# (U1x, U1y, U1z)

My line:
  S + V t

Hailstone lines:
  H# + U# t

We can decompose this into axes:
  Sx + Vx t1 = H1x + U1x t1
  Sy + Vy t2 = H1y + U1y t2
  Sz + Vz t3 = H1z + U1z t3
  Sx + Vx t4 = H2x + U2x t4
  ...
  Sz + Vz tM = HNx + UNx tM

This doesn't tell us a whole lot, but we do know that ALL of these are integers.

Sx + Vx t1 = H1x + U1x t1

Scanning the input file, the individual velocity vectors are in the range [-500, 500] or so.
So let's say the bound for V is [-1000, 1000] as a guess.
These numbers are relatively small, so we can use their integer-ness as a hint.

We can scan the full range of all possible values of V (Vx, Vy, Vz separately) and see which ones lead to viable outcomes.
Sx + Vx t1 = H1x + U1x t1
-> Sx = H1x + t1 (U1x - Vx)

For example, guess Vx = 20.
  Hailstone 1: H1x = 248315803897794, U1x = -89
    Sx = 248315803897794 + t1 (-89 - 20)
    --> If Vx=20, the range of legal values for Sx (relating to H1) is [248315803897794, 248315803897794 - 109, 248315803897794 - 2*109, ...]
  Hailstone 2: H2x = 332497633176671, U2x = -120
    Sx = 332497633176671 + t2 (-120 - 20)
    --> If Vx=20, the range of legal values for Sx (relating to H2) is [332497633176671, 332497633176671 - 140, 332497633176671 - 2*140, ...]
  Hailstone 3: H3x = 362310144548603, U3x = 80
    Sx = 362310144548603 + t2 (80 - 20)
    --> If Vx=20, the range of legal values for Sx (relating to H3) is [362310144548603, 362310144548603 + 60, 362310144548603 + 2*60, ...]

These ranges should intersect, producing a combined sequence in the [0, 999999999999999] range (based on the inputs).
Based on the problem description, it should resolve to a single entry.

If we flip the sequences to always be incrementing from 0:
  H1: [18, 18+109, 18+(109*2), ...]
  H2: [11, 11+140, 11+(140*2), ...]
  H3: [23, 23+60, 23+(60*2), ...]
--
  H1: x = 18 mod 109
  H2: x = 11 mod 140
  H3: x = 23 mod 60
  ...

Now, we bust out the Chinese Remainder Theorem (I hate this thing).
We can use CRT to determine a solution if and only if all of the divisors are relatively prime.
So, in order to use CRT we need to pick a value of Vx such that all |U#x - Vx| are relatively prime (and reduce to the same LHS if the modulus is identical).
Let's try it and see what we get!
It turns out I didn't even need to go so far... simply testing that the LHS's are the same when the modulus is identical is enough to uniquely identify (Vx, Vy, Vz) within the range [-1000,1000].

We can factorize the number as we go to make the CRT work.

*/

use std::{collections::HashMap, fs};
use itertools::Itertools;
use num::{BigInt, One, ToPrimitive, Zero};
use num_prime::nt_funcs::factorize;

#[derive(PartialEq, Eq, Clone, Copy, Hash, Debug)]
struct Pt { x: i64, y: i64, z: i64 }

#[derive(PartialEq, Eq, Clone, Copy, Hash, Debug)]
struct Hailstone { pos: Pt, vel: Pt }

fn parse_pt(s: &str) -> Pt {
    let s = s.split(", ").collect_vec();
    Pt { x: s[0].parse().unwrap(), y: s[1].parse().unwrap(), z: s[2].parse().unwrap() }
}

fn parse_hailstone(s: &str) -> Hailstone {
    let (a, b) = s.split_once(" @ ").unwrap();
    Hailstone { pos: parse_pt(a), vel: parse_pt(b) }
}

fn xy_intersects_within_region(a: &Hailstone, b: &Hailstone, min_pos: i64, max_pos: i64) -> bool {
    // https://stackoverflow.com/questions/2931573/determining-if-two-rays-intersect
    let (x1, x2) = (a.pos.x as i128, (a.pos.x + a.vel.x) as i128);
    let (y1, y2) = (a.pos.y as i128, (a.pos.y + a.vel.y) as i128);
    let (x3, x4) = (b.pos.x as i128, (b.pos.x + b.vel.x) as i128);
    let (y3, y4) = (b.pos.y as i128, (b.pos.y + b.vel.y) as i128);
    let denom = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
    if denom == 0 {
        false
    } else {
        let px = (((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / denom) as i64;
        let py = (((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / denom) as i64;
        if a.vel.x.signum() != (px - a.pos.x).signum() || a.vel.y.signum() != (py - a.pos.y).signum() || b.vel.x.signum() != (px - b.pos.x).signum() || b.vel.y.signum() != (py - b.pos.y).signum() {
            false // in the past
        } else {
            px >= min_pos && px <= max_pos && py >= min_pos && py <= max_pos
        }
    }
}

// Adapted from https://rosettacode.org/wiki/Chinese_remainder_theorem#Rust (with a ton of ugly .clone()s for bigint)
fn chinese_remainder(congruences: &HashMap<i64, i64>) -> BigInt {
    let prod = congruences.keys().product::<BigInt>();
    fn egcd(a: BigInt, b: BigInt) -> (BigInt, BigInt, BigInt) {
        if a.is_zero() {
            (b, BigInt::zero(), BigInt::one())
        } else {
            let (g, x, y) = egcd(b.clone() % a.clone(), a.clone());
            (g, y - (b.clone() / a.clone()) * x.clone(), x.clone())
        }
    }

    fn mod_inv(x: BigInt, n: BigInt) -> BigInt {
        let (g, x, _) = egcd(x.clone(), n.clone());
        assert!(g.is_one());
        (x.clone() % n.clone() + n.clone()) % n.clone()
    }

    let mut sum = BigInt::zero();

    for (&modulus, &residue) in congruences.iter() {
        let p = prod.clone() / modulus;
        sum += residue * mod_inv(p.clone(), BigInt::from(modulus)) * p
    }

    sum % prod
}

fn solve_for_s_given_v(v: i64, hailstones: &Vec<Hailstone>, getter: fn(&Pt) -> i64) -> Option<i64> {
    let mut congruences = HashMap::new();
    for h in hailstones.iter() {
        let modulus = (v - getter(&h.vel)).abs();
        if modulus != 0 {
            for (factor, _) in factorize(modulus as u64) {
                let factor = factor as i64;
                let lhs = getter(&h.pos) % factor;
                if let Some(prev) = congruences.get(&factor) {
                    if *prev != lhs {
                        return None;
                    }
                } else {
                    congruences.insert(factor, lhs);
                }
            }
        }
    }
    chinese_remainder(&congruences).to_i64()
}

fn solve_for_s(hailstones: &Vec<Hailstone>, getter: fn(&Pt) -> i64) -> i64 {
    // There should be only a single guess at V that leads to a valid S.
    (-1000..1000).filter_map(|v| solve_for_s_given_v(v, hailstones, getter)).exactly_one().unwrap()
}

fn main() {
    let input = fs::read_to_string("input/day-24.txt").unwrap();
    let hailstones = input.lines().map(parse_hailstone).collect_vec();

    let p1_result = hailstones.iter().tuple_combinations().filter(|(a, b)| xy_intersects_within_region(a, b, 200000000000000, 400000000000000)).count();
    println!("Part 1: {p1_result}");

    let sx = solve_for_s(&hailstones, |p| p.x);
    let sy = solve_for_s(&hailstones, |p| p.y);
    let sz = solve_for_s(&hailstones, |p| p.z);
    println!("Part 2: {}", sx + sy + sz);
}
