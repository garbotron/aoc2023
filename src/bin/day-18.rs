/*
The usual part 2 switcheroo.
The basic flood fill I used for part 1 is no longer bueno.
Instead we'll break the shape up into boxes (including boxes for the edge itself), then flood fill those.
*/

use std::{fs, collections::HashSet};
use itertools::Itertools;
use regex::Regex;

#[derive(PartialEq, Eq, Clone, Copy, Hash, Debug)]
enum Dir { U, D, L, R }

#[derive(PartialEq, Eq, Clone, Hash, Debug)]
struct Instruction { dir: Dir, len: i64, color: String }

fn parse_instruction(s: &str) -> Instruction {
    let re = Regex::new(r"(U|D|L|R) ([0-9]+) \(#([0-9a-f]*)\)").unwrap();
    let cap = re.captures(s).unwrap();
    Instruction {
        dir: match cap.get(1).unwrap().as_str() {
            "U" => Dir::U,
            "D" => Dir::D,
            "L" => Dir::L,
            "R" => Dir::R,
            _ => panic!(),
        },
        len: cap.get(2).unwrap().as_str().parse().unwrap(),
        color: cap.get(3).unwrap().as_str().to_string(),
    }
}

fn decode(i: &Instruction) -> Instruction {
    Instruction {
        dir: match &i.color[5..] {
            "0" => Dir::R,
            "1" => Dir::D,
            "2" => Dir::L,
            "3" => Dir::U,
            _ => panic!(),
        },
        len: i64::from_str_radix(&i.color[..5], 16).unwrap(),
        color: "".to_string(),
    }
}

fn solve(instructions: &Vec<Instruction>) -> i64 {
    let (mut cur_x, mut cur_y) = (0, 0);

    // First split trench borders into boxes (1 wide/tall).
    let mut trenches = HashSet::new();
    for i in instructions.iter() {
        trenches.insert((cur_x, cur_y, 1, 1));
        match i.dir {
            Dir::U => {
                cur_y -= i.len;
                trenches.insert((cur_x, cur_y + 1, 1, i.len - 1));
            },
            Dir::D => {
                trenches.insert((cur_x, cur_y + 1, 1, i.len - 1));
                cur_y += i.len;
            },
            Dir::L => {
                cur_x -= i.len;
                trenches.insert((cur_x + 1, cur_y, i.len - 1, 1));
            },
            Dir::R => {
                trenches.insert((cur_x + 1, cur_y, i.len - 1, 1));
                cur_x += i.len;
            },
        }
    }
    trenches.retain(|&(_, _, w, h)| w > 0 && h > 0);

    // Calculate the breakpoints on the x and y axes.
    let x_breakpoints = trenches.iter().flat_map(|(x, _, _, _)| [*x, *x + 1]).unique().sorted().collect_vec();
    let y_breakpoints = trenches.iter().flat_map(|(_, y, _, _)| [*y, *y + 1]).unique().sorted().collect_vec();

    // Create a map of which inter-breakpoint regions have been dug.
    let mut dug = HashSet::new();
    for (i, (&x1, &x2)) in x_breakpoints.iter().tuple_windows().enumerate() {
        for (j, (&y1, &y2)) in y_breakpoints.iter().tuple_windows().enumerate() {
            if trenches.iter().any(|&(x, y, w, h)| x1 >= x && y1 >= y && x2 <= x + w && y2 <= y + h) {
                dug.insert((i as i32, j as i32));
            }
        }
    }

    // Find a single rectangle that is known to be inside the trench, then flood outward from there.
    let left_wall_y = dug.iter().filter(|(x, _)| *x == 0).map(|(_, y)| *y).min().unwrap();
    let mut to_fill = vec![(1, left_wall_y + 1)];
    while let Some((x, y)) = to_fill.pop() {
        if dug.insert((x, y)) {
            to_fill.append(&mut vec![(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)]);
        }
    }

    dug.iter().map(|&(i, j)| (x_breakpoints[i as usize + 1] - x_breakpoints[i as usize]) * (y_breakpoints[j as usize + 1] - y_breakpoints[j as usize])).sum()
}

fn main() {
    let input = fs::read_to_string("input/day-18.txt").unwrap();
    let instructions = input.lines().map(parse_instruction).collect_vec();

    println!("Part 1: {}", solve(&instructions));
    println!("Part 2: {}", solve(&instructions.iter().map(decode).collect_vec()));
}
