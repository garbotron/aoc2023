use std::{fs, collections::{HashMap, HashSet}};
use regex::Regex;

enum Cell {
    PartOfNumber(i32, usize),
    Symbol(char),
}

fn parse_cells(input: &str) -> HashMap<(i32, i32), Cell> {
    let mut map = HashMap::new();
    for (y, line) in input.lines().enumerate() {
        for m in Regex::new("[0-9]+").unwrap().find_iter(line) {
            let num = m.as_str().parse().unwrap();
            for x in m.start()..(m.start() + m.len()) {
                map.insert((x as i32, y as i32), Cell::PartOfNumber(num, x - m.start()));
            }
        }
        for m in Regex::new(r"[^0-9\.]").unwrap().find_iter(line) {
            map.insert((m.start() as i32, y as i32), Cell::Symbol(m.as_str().chars().next().unwrap()));
        }
    }
    map
}

fn extract_part_number((x, y): (i32, i32), cells: &HashMap<(i32, i32), Cell>) -> Option<i32> {
    if let Some(Cell::PartOfNumber(num, 0)) = cells.get(&(x, y)) {
        for x in (x - 1)..=(x + num.to_string().len() as i32) {
            for y in [y - 1, y, y + 1] {
                if let Some(Cell::Symbol(_)) = cells.get(&(x, y)) {
                    return Some(*num);
                }
            }
        }
    }
    None
}

fn get_number_start(x: i32, y: i32, cells: &HashMap<(i32, i32), Cell>) -> Option<(i32, i32, i32)> {
    match cells.get(&(x, y)) {
        Some(Cell::PartOfNumber(num, 0)) => Some((x, y, *num)),
        Some(Cell::PartOfNumber(_, _)) => get_number_start(x - 1, y, cells),
        _ => None,
    }
}

fn extract_gear_ratio((x, y): (i32, i32), cells: &HashMap<(i32, i32), Cell>) -> Option<i64> {
    let mut adjacent_nums = HashSet::new();
    if let Some(Cell::Symbol('*')) = cells.get(&(x, y)) {
        for x in [x - 1, x, x + 1] {
            for y in [y - 1, y, y + 1] {
                if let Some((x, y, num)) = get_number_start(x, y, cells) {
                    adjacent_nums.insert((x, y, num));
                }
            }
        }
    }
    if adjacent_nums.len() == 2 {
        Some(adjacent_nums.iter().fold(1, |x, (_, _, y)| x * (*y as i64)))
    } else {
        None
    }
}

fn main() {
    let input = fs::read_to_string("input/day-03.txt").unwrap();
    let cells = parse_cells(&input);
    println!("Part 1: {}", cells.keys().filter_map(|xy| extract_part_number(*xy, &cells)).sum::<i32>());
    println!("Part 2: {}", cells.keys().filter_map(|xy| extract_gear_ratio(*xy, &cells)).sum::<i64>());
}
