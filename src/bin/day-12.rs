/* The old switch em up! Part 2 necessitates a very different data structure than I used for part 1. Oh well. */

use std::{fs, collections::HashMap};
use itertools::Itertools;

#[derive(Debug, Eq, PartialEq, Hash, Clone, Copy)]
enum Spring { Damaged, Operational, Unknown }

#[derive(Debug, Eq, PartialEq, Hash, Clone, Default)]
struct State {
    spring_idx: usize,
    region_idx: usize,
    region_remaining: usize,
    separator_required: bool,
}

#[derive(Debug)]
struct Invariant {
    springs: Vec<Spring>,
    regions: Vec<usize>,
}

fn consume_operational_spring(state: &State, inv: &Invariant, cache: &mut HashMap<State, u64>) -> u64 {
    // If there are more damaged springs in the current region, we're boned. Otherwise, skip it and move on.
    if state.region_remaining > 0 {
        0
    } else {
        num_permutations(&State { spring_idx: state.spring_idx + 1, separator_required: false, ..*state }, inv, cache)
    }
}

fn consume_damaged_spring(state: &State, inv: &Invariant, cache: &mut HashMap<State, u64>) -> u64 {
    // If there are more damaged springs in the current region, we're gold. Otherwise we need to start the next region.
    if state.separator_required {
        0
    } else if state.region_remaining > 0 {
        num_permutations(&State { spring_idx: state.spring_idx + 1, region_remaining: state.region_remaining - 1, separator_required: state.region_remaining == 1, ..*state }, inv, cache)
    } else if let Some(&next_region) = inv.regions.get(state.region_idx) {
        num_permutations(&State { region_remaining: next_region, region_idx: state.region_idx + 1, ..*state }, inv, cache)
    } else {
        0
    }
}

fn num_permutations(state: &State, inv: &Invariant, cache: &mut HashMap<State, u64>) -> u64 {
    if let Some(cached) = cache.get(state) {
        return *cached;
    } else {
        let ret = match inv.springs.get(state.spring_idx) {
            None if state.region_remaining == 0 && state.region_idx == inv.regions.len() => 1,
            None => 0,
            Some(Spring::Operational) => consume_operational_spring(state, inv, cache),
            Some(Spring::Damaged) => consume_damaged_spring(state, inv, cache),
            Some(Spring::Unknown) => consume_operational_spring(state, inv, cache) + consume_damaged_spring(state, inv, cache),
        };
        cache.insert(state.clone(), ret);
        ret
    }
}

fn main() {
    let input = fs::read_to_string("input/day-12.txt").unwrap();

    let mut invariants_p1 = vec![];
    for line in input.lines() {
        let (pre_space, post_space) = line.split_once(' ').unwrap();
        let springs = pre_space.chars().map(|x| match x {
            '#' => Spring::Damaged,
            '.' => Spring::Operational,
            '?' => Spring::Unknown,
            _ => panic!(),
        }).collect_vec();
        let regions = post_space.split(',').map(|x| x.parse().unwrap()).collect_vec();
        invariants_p1.push(Invariant { springs, regions });
    }

    let invariants_p2 = invariants_p1.iter().map(|x| {
        Invariant {
            springs: vec![x.springs.clone(); 5].join(&Spring::Unknown),
            regions: vec![x.regions.clone(); 5].concat(),
        }
    }).collect_vec();

    println!("Part 1: {}", invariants_p1.iter().map(|x| num_permutations(&State::default(), x, &mut HashMap::new())).sum::<u64>());
    println!("Part 2: {}", invariants_p2.iter().map(|x| num_permutations(&State::default(), x, &mut HashMap::new())).sum::<u64>());
}
