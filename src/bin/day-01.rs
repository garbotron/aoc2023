use std::fs;

fn numberize(s: &str, map: &Vec<(&str, i32)>) -> Vec<i32> {
    let mut ret = Vec::new();
    for i in 0..s.len() {
        for &(k, v) in map.iter() {
            if s[i..].starts_with(k) {
                ret.push(v);
            }
        }
    }
    ret
}

fn calibration_value(s: &str, map: &Vec<(&str, i32)>) -> i32 {
    let nums = numberize(s, map);
    nums.first().unwrap() * 10 + nums.last().unwrap()
}

fn calibration_value_sum(s: &str, map:& Vec<(&str, i32)>) -> i32 {
    s.lines().map(|s| calibration_value(s, map)).sum()
}

fn main() {
    let input = fs::read_to_string("input/day-01.txt").unwrap();

    let map_digits = vec![("0", 0), ("1", 1), ("2", 2), ("3", 3), ("4", 4), ("5", 5), ("6", 6), ("7", 7), ("8", 8), ("9", 9)];
    println!("Part 1: {}", calibration_value_sum(&input, &map_digits));

    let map_words = vec![("one", 1), ("two", 2), ("three", 3), ("four", 4), ("five", 5), ("six", 6), ("seven", 7), ("eight", 8), ("nine", 9)];
    println!("Part 2: {}", calibration_value_sum(&input, &[map_digits, map_words].concat()));
}
