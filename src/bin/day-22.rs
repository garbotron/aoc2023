/*

The crux of this seems to be maintaining a fast data store for checking if a position is occupied.
The coordinates don't exceed ~300 in all cases so we can just keep a running hash of occupied (x, y, z) positions.
It could be faster, but it works.

*/

use std::{fs, collections::{HashSet, HashMap}};
use itertools::Itertools;

#[derive(Debug, Clone)]
struct Cube {
    cnr1: (i64, i64, i64),
    cnr2: (i64, i64, i64),
}

#[derive(Debug, Clone)]
struct State {
    cubes: Vec<Cube>,
    settled: HashSet<usize>,
    occupied: HashMap<(i64, i64, i64), usize>,
}

fn parse_coord(s: &str) -> (i64, i64, i64) {
    let s = s.split(',').collect_vec();
    (s[0].parse().unwrap(), s[1].parse().unwrap(), s[2].parse().unwrap())
}

fn parse_cube(s: &str) -> Cube {
    let (a, b) = s.split_once('~').unwrap();
    Cube { cnr1: parse_coord(a), cnr2: parse_coord(b) }
}

fn parse_state(s: &str) -> State {
    let mut state = State { cubes: Vec::new(), settled: HashSet::new(), occupied: HashMap::new() };
    for l in s.lines() {
        let cube = parse_cube(l);
        state.cubes.push(cube);
        let idx = state.cubes.len() - 1;
        add_cube_to_hash(&mut state, idx);
    }
    state
}

fn remove_cube_from_hash(state: &mut State, cube_idx: usize) {
    let cube = &state.cubes[cube_idx];
    for x in cube.cnr1.0..=cube.cnr2.0 {
        for y in cube.cnr1.1..=cube.cnr2.1 {
            for z in cube.cnr1.2..=cube.cnr2.2 {
                state.occupied.remove(&(x, y, z));
            }
        }
    }
}

fn add_cube_to_hash(state: &mut State, cube_idx: usize) {
    let cube = &state.cubes[cube_idx];
    for x in cube.cnr1.0..=cube.cnr2.0 {
        for y in cube.cnr1.1..=cube.cnr2.1 {
            for z in cube.cnr1.2..=cube.cnr2.2 {
                state.occupied.insert((x, y, z), cube_idx);
            }
        }
    }
}

fn move_cube_down(state: &mut State, cube_idx: usize, dist: i64) {
    remove_cube_from_hash(state, cube_idx);
    state.cubes[cube_idx].cnr1.2 -= dist;
    state.cubes[cube_idx].cnr2.2 -= dist;
    add_cube_to_hash(state, cube_idx);
}

fn how_far_can_fall(state: &State, cube_idx: usize) -> Option<i64> {
    for delta in 1.. {
        let z = state.cubes[cube_idx].cnr1.2 - delta;
        if z == 0 {
            return Some(delta - 1);
        }
        for x in state.cubes[cube_idx].cnr1.0..=state.cubes[cube_idx].cnr2.0 {
            for y in state.cubes[cube_idx].cnr1.1..=state.cubes[cube_idx].cnr2.1 {
                if let Some(c) = state.occupied.get(&(x, y, z)) {
                    if *c != cube_idx {
                        if state.settled.contains(c) {
                            return Some(delta - 1);
                        } else {
                            return None;
                        }
                    }
                }
            }
        }
    }
    panic!()
}

fn settle_next(state: &mut State) -> usize {
    // Find a cube that, if it falls, will hit a settled cube or the ground before hitting any of the other unseltted cubes.
    let unsettled = (0..state.cubes.len()).filter(|x| !state.settled.contains(x)).collect_vec();
    for cube_idx in unsettled {
        match how_far_can_fall(state, cube_idx) {
            None => { },
            Some(0) => {
                state.settled.insert(cube_idx);
                return 0; // 0 blocks fell
            },
            Some(d) => {
                state.settled.insert(cube_idx);
                move_cube_down(state, cube_idx, d);
                return 1; // 1 block fell
            },
        }
    }
    panic!();
}

fn settle_all(state: &mut State) -> usize {
    let mut ret = 0;
    while state.settled.len() < state.cubes.len() {
        ret += settle_next(state);
    }
    ret
}

fn num_blocks_to_fall_if_removed(state: &State, cube_idx: usize) -> usize {
    let mut state = state.clone();
    state.settled.clear();
    remove_cube_from_hash(&mut state, cube_idx);
    settle_all(&mut state)
}

fn main() {
    let input = fs::read_to_string("input/day-22.txt").unwrap();

    let mut init_state = parse_state(&input);

    settle_all(&mut init_state);

    println!("Part 1: {}", (0..init_state.cubes.len()).filter(|i| num_blocks_to_fall_if_removed(&init_state, *i) == 0).count());
    println!("Part 2: {}", (0..init_state.cubes.len()).map(|i| num_blocks_to_fall_if_removed(&init_state, i)).sum::<usize>());
}
