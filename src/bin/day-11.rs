/*
At first glance this seems like a pathfinding problem but it's really a lot simpler than that.
There will never be any need to path "around" an expanded area of space, since you'll ultimately have to go through it in any case.
So in the end it's just manhattan distance + offsets.
*/

use std::fs;
use itertools::Itertools;

fn dist((ax, ay): (u64, u64), (bx, by): (u64, u64), expanded_x: &Vec<u64>, expanded_y: &Vec<u64>, expansion_factor: u64) -> u64 {
    let big_x_count = expanded_x.iter().skip_while(|a| **a < ax.min(bx)).take_while(|a| **a <= ax.max(bx)).count() as u64;
    let big_y_count = expanded_y.iter().skip_while(|a| **a < ay.min(by)).take_while(|a| **a <= ay.max(by)).count() as u64;
    (ax.abs_diff(bx) - big_x_count) + (ay.abs_diff(by) - big_y_count) + ((big_x_count + big_y_count) * expansion_factor)
}

fn total_dist(galaxies: &Vec<(u64, u64)>, expanded_x: &Vec<u64>, expanded_y: &Vec<u64>, expansion_factor: u64) -> u64 {
    let mut ret = 0;
    for i in 0..galaxies.len() {
        for j in (i+1)..galaxies.len() {
            ret += dist(galaxies[i], galaxies[j], expanded_x, expanded_y, expansion_factor);
        }
    }
    ret
}

fn main() {
    let input = fs::read_to_string("input/day-11.txt").unwrap();
    let lines = input.lines().collect_vec();
    let height = lines.len() as u64;
    let width = lines[0].len() as u64;

    let mut galaxies = vec![];
    for (y, line) in lines.iter().enumerate() {
        for (x, ch) in line.chars().enumerate() {
            if ch == '#' {
                galaxies.push((x as u64, y as u64));
            }
        }
    }

    let expanded_x = (0..width).filter(|&x| galaxies.iter().all(|&(a, _)| a != x)).collect_vec();
    let expanded_y = (0..height).filter(|&y| galaxies.iter().all(|&(_, a)| a != y)).collect_vec();

    println!("Part 1: {}", total_dist(&galaxies, &expanded_x, &expanded_y, 2));
    println!("Part 2: {}", total_dist(&galaxies, &expanded_x, &expanded_y, 1_000_000));
}
