use std::{fs, collections::{HashMap, VecDeque}};
use itertools::Itertools;

enum ModuleState<'a> {
    Broadcaster,
    FlipFlop(bool),
    Conjunction(HashMap<&'a str, bool>),
}

struct Module<'a> {
    name: &'a str,
    state: ModuleState<'a>,
    outputs: Vec<&'a str>,
    inputs: Vec<&'a str>,
}

struct DirectedPulse<'a> {
    hi: bool,
    from: &'a str,
    to: &'a str,
}

struct GlobalState<'a> {
    pulse_queue: VecDeque<DirectedPulse<'a>>,
    hi_total: usize,
    lo_total: usize,
}

impl<'a> Module<'a> {
    fn parse(s: &'a str) -> Module {
        let (name, dests) = s.split_once(" -> ").unwrap();
        let destinations = dests.split(", ").collect_vec();
        let (name, state) = match name {
            "broadcaster" => (name, ModuleState::Broadcaster),
            x if x.starts_with('%') => (name.trim_start_matches('%'), ModuleState::FlipFlop(false)),
            x if x.starts_with('&') => (name.trim_start_matches('&'), ModuleState::Conjunction(HashMap::new())),
            _ => panic!(),
        };
        Module { name, state, outputs: destinations, inputs: Vec::new() }
    }

    fn send_pulse(&self, pulse: bool, global: &mut GlobalState<'a>) {
        for d in self.outputs.iter() {
            global.pulse_queue.push_back(DirectedPulse { hi: pulse, from: self.name, to: d });
            if pulse {
                global.hi_total += 1;
            } else {
                global.lo_total += 1;
            }
        }
    }

    fn process_pulse(&mut self, pulse: &DirectedPulse<'a>, global: &mut GlobalState<'a>) {
        let pulse_to_send = match &mut self.state {
            ModuleState::Broadcaster => Some(pulse.hi),
            ModuleState::FlipFlop(state) if !pulse.hi => {
                *state = !*state;
                Some(*state)
            },
            ModuleState::Conjunction(cache) => {
                cache.insert(pulse.from, pulse.hi);
                let all_hi = self.inputs.iter().all(|x| *cache.get(x).unwrap_or(&false));
                Some(!all_hi)
            },
            _ => None,
        };
        if let Some(p) = pulse_to_send {
            self.send_pulse(p, global);
        }
    }
}

fn parse_modules<'a>(input: &'a str) -> HashMap<&'a str, Module<'a>> {
    let mut modules = HashMap::new();
    for line in input.lines() {
        let module = Module::parse(line);
        modules.insert(module.name, module);
    }
    modules.insert("button", Module { name: "button", state: ModuleState::Broadcaster, outputs: vec!["broadcaster"], inputs: Vec::new() });

    // Calculate the inputs based on the parsed outputs.
    // Rust makes this tricky due to borrowing multiple hashset elements at once, so we need to make a copy of the associations.
    let output_map = modules.values().flat_map(|m| m.outputs.iter().map(|&o| (m.name, o))).collect_vec();
    for (m, o) in output_map {
        if let Some(x) = modules.get_mut(o) {
            x.inputs.push(m);
        }
    }
    modules
}

fn main() {
    let input = fs::read_to_string("input/day-20.txt").unwrap();

    let mut modules = parse_modules(&input);
    let mut global = GlobalState { pulse_queue: VecDeque::new(), hi_total: 0, lo_total: 0 };
    for _ in 0..1000 {
        modules.get_mut("button").unwrap().send_pulse(false, &mut global);
        while let Some(p) = global.pulse_queue.pop_front() {
            if let Some(module) = modules.get_mut(p.to) {
                module.process_pulse(&p, &mut global);
            }
        }
    }
    println!("Part 1: {}", global.hi_total * global.lo_total);

    /*
    For part 2, we'll need to actually study the input and use math to figure out how long it would take.

    "rx" will trip only when all of the inputs to "kj" are high: [ln, dr, zx, vn].
    Let's check the periods for these...

    ln: 4003,8006,12009,16012,20015,24018,28021,32024,36027,40030,44033,48036,52039,56042,60045,64048,68051,72054,76057,80060,84063,88066,92069,96072,
    dr: 3863,7726,11589,15452,19315,23178,27041,30904,34767,38630,42493,46356,50219,54082,57945,61808,65671,69534,73397,77260,81123,84986,88849,92712,96575,
    zx: 3989,7978,11967,15956,19945,23934,27923,31912,35901,39890,43879,47868,51857,55846,59835,63824,67813,71802,75791,79780,83769,87758,91747,95736,99725,
    vn: 3943,7886,11829,15772,19715,23658,27601,31544,35487,39430,43373,47316,51259,55202,59145,63088,67031,70974,74917,78860,82803,86746,90689,94632,98575,

    These are all multiples of a single prime number. The LCM of these numbers will give us the intersection point.
    LCM(4003,3863,3989,3943) = 243221023462303
    */
    println!("Part 2: 243221023462303");
}
