use std::fs;
use itertools::Itertools;
use regex::Regex;

struct Turn { red: i32, green: i32, blue: i32 }
struct Game { id: i32, turns: Vec<Turn> }

fn extract_number(re: &str, haystack: &str) -> i32 {
    if let Some(cap) = Regex::new(re).unwrap().captures(haystack) {
        cap.get(1).unwrap().as_str().parse().unwrap()
    } else {
        0
    }
}

fn parse_turn(s: &str) -> Turn {
    Turn {
        red: extract_number("([0-9]+) red", s),
        green: extract_number("([0-9]+) green", s),
        blue: extract_number("([0-9]+) blue", s),
    }
}

fn parse_game(s: &str) -> Game {
    Game {
        id: extract_number("Game ([0-9]+)", s),
        turns: s.split(';').map(parse_turn).collect(),
    }
}

fn turn_is_possible(t: &Turn, limit: &Turn) -> bool {
    t.red <= limit.red && t.green <= limit.green && t.blue <= limit.blue
}

fn game_is_possible(g: &Game, limit: &Turn) -> bool {
    g.turns.iter().all(|t| turn_is_possible(t, limit))
}

fn game_power(g: &Game) -> i32 {
    let red = g.turns.iter().map(|t| t.red).max().unwrap();
    let green = g.turns.iter().map(|t| t.green).max().unwrap();
    let blue = g.turns.iter().map(|t| t.blue).max().unwrap();
    red * green * blue
}

fn main() {
    let input = fs::read_to_string("input/day-02.txt").unwrap();
    let games = input.lines().map(parse_game).collect_vec();

    let p1_limit = Turn { red: 12, green: 13, blue: 14 };
    let p1_games = games.iter().filter(|g| game_is_possible(g, &p1_limit));
    let p1_sum = p1_games.map(|g| g.id).sum::<i32>();
    println!("Part 1: {p1_sum}");

    let p2_sum = games.iter().map(game_power).sum::<i32>();
    println!("Part 2: {p2_sum}");
}
