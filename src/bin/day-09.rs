use std::fs;

use itertools::Itertools;

fn next_elem(seq: &Vec<i64>) -> i64 {
    if seq.iter().all(|x| *x == 0) {
        0
    } else {
        let deltas = seq.iter().tuple_windows().map(|(x, y)| y - x).collect_vec();
        *seq.last().unwrap() + next_elem(&deltas)
    }
}

fn prev_elem(seq: &Vec<i64>) -> i64 {
    if seq.iter().all(|x| *x == 0) {
        0
    } else {
        let deltas = seq.iter().tuple_windows().map(|(x, y)| y - x).collect_vec();
        *seq.first().unwrap() - prev_elem(&deltas)
    }
}

fn main() {
    let input = fs::read_to_string("input/day-09.txt").unwrap().lines()
        .map(|x| x.split_whitespace().map(|y| y.parse::<i64>().unwrap()).collect_vec())
        .collect_vec();

    println!("Part 1: {}", input.iter().map(next_elem).sum::<i64>());
    println!("Part 2: {}", input.iter().map(prev_elem).sum::<i64>());
}
