/*

Once again part 1 can be modeled as a simple case of part 2.

General algorithm:

Eventually, given a starting location, the path will start cycling.
In order to find that cycle, we keep track of each node we've visited along with the position within the path sequence.
When we hit a duplicate, we know we've started cycling.

This could be a really difficult problem depending on how many victory states were encountered per cycle or before
the start of the cycle, but it turns out that...

GNA => CycleInfo { cycle_start: 2, cycle_len: 20093, victories: [20093] }
MXA => CycleInfo { cycle_start: 2, cycle_len: 14999, victories: [14999] }
XHA => CycleInfo { cycle_start: 2, cycle_len: 17263, victories: [17263] }
FCA => CycleInfo { cycle_start: 3, cycle_len: 12169, victories: [12169] }
AAA => CycleInfo { cycle_start: 2, cycle_len: 22357, victories: [22357] }
VVA => CycleInfo { cycle_start: 2, cycle_len: 13301, victories: [13301] }

So, the first victory happens at turn X, then again after another X turns.
They made this easy for us. We just need to find the LCM of all of these victory positions.

*/

use std::{fs, collections::HashMap};
use itertools::Itertools;
use num::Integer;
use regex::Regex;

#[derive(Debug)]
#[allow(dead_code)]
struct CycleInfo {
    cycle_start: u64,
    cycle_len: u64,
    victories: Vec<u64>,
}

fn find_cycle_info(start: &str, path: &Vec<char>, network: &HashMap<&str, (&str, &str)>) -> CycleInfo {
    let mut seen_map = HashMap::new();
    let mut steps = 0;
    let mut cur = start;
    let mut path_idx = 0;
    let mut victories = Vec::new();
    loop {
        if let Some(prev_cycle) = seen_map.insert((cur, path_idx), steps) {
            return CycleInfo { cycle_start: prev_cycle, cycle_len: steps - prev_cycle, victories };
        }
        if cur.ends_with("Z") {
            victories.push(steps);
        }
        match path[path_idx] {
            'L' => (cur, _) = *network.get(cur).unwrap(),
            'R' => (_, cur) = *network.get(cur).unwrap(),
            _ => panic!(),
        }
        steps += 1;
        path_idx = (path_idx + 1) % path.len();
    }
}

fn solve(starting_points: &Vec<&str>, path: &Vec<char>, network: &HashMap<&str, (&str, &str)>) -> u64 {
    starting_points
        .iter()
        .map(|s| find_cycle_info(s, path, network))
        .map(|x| x.victories[0])
        .fold(1, |acc, x| acc.lcm(&x))
}

fn main() {
    let input = fs::read_to_string("input/day-08.txt").unwrap();
    let lines = input.lines().collect_vec();
    let path = lines[0].chars().collect_vec();
    let mut network = HashMap::new();
    let re = Regex::new(r"(.*) = \((.*), (.*)\)").unwrap();
    for line in lines.iter().skip(2) {
        let m = re.captures(line).unwrap();
        let src = m.get(1).unwrap().as_str();
        let left = m.get(2).unwrap().as_str();
        let right = m.get(3).unwrap().as_str();
        network.insert(src, (left, right));
    }

    println!("Part 1: {}", solve(&vec!["AAA"], &path, &network));

    let starting_points = network.keys().filter(|x| x.ends_with("A")).copied().collect_vec();
    println!("Part 2: {}", solve(&starting_points, &path, &network));
}
