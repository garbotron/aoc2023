use std::fs;
use itertools::Itertools;

#[derive(Clone, Debug)]
struct Map {
    width: usize,
    height: usize,
    rocks: Vec<Vec<bool>>,
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
enum ReflectionLine {
    Horizontal(usize),
    Vertical(usize),
}

fn parse_map(s: &str) -> Map {
    let lines = s.trim().lines().collect_vec();
    let mut ret = Map { width: lines[0].len(), height: lines.len(), rocks: Vec::new() };
    for l in lines.iter() {
        let mut row = Vec::new();
        for ch in l.chars() {
            row.push(ch == '#');
        }
        ret.rocks.push(row);
    }
    ret
}

fn is_horiz_reflection_line(map: &Map, y: usize) -> bool {
    for d in 0..=y {
        let my_y = y - d;
        let reflected_y = y + 1 + d;
        if reflected_y < map.height {
            for x in 0..map.width {
                if map.rocks[my_y][x] != map.rocks[reflected_y][x] {
                    return false;
                }
            }
        }
    }
    true
}

fn is_vert_reflection_line(map: &Map, x: usize) -> bool {
    for d in 0..=x {
        let my_x = x - d;
        let reflected_x = x + 1 + d;
        if reflected_x < map.width {
            for y in 0..map.height {
                if map.rocks[y][my_x] != map.rocks[y][reflected_x] {
                    return false;
                }
            }
        }
    }
    true
}

fn solve_many(map: &Map) -> Vec<ReflectionLine> {
    let h_lines = (0..(map.height - 1)).filter(|y| is_horiz_reflection_line(map, *y)).map(ReflectionLine::Horizontal);
    let v_lines = (0..(map.width - 1)).filter(|x| is_vert_reflection_line(map, *x)).map(ReflectionLine::Vertical);
    h_lines.chain(v_lines).collect()
}

fn solve_one(map: &Map) -> ReflectionLine {
    *solve_many(map).iter().exactly_one().unwrap()
}

fn score(line: ReflectionLine) -> usize {
    match line {
        ReflectionLine::Horizontal(x) => (x + 1) * 100,
        ReflectionLine::Vertical(x) => x + 1,
    }
}

fn unsmudge(map: &Map) -> ReflectionLine {
    let initial_solution = solve_one(map);
    let mut mod_map = map.clone();
    for x in 0..map.width {
        for y in 0..map.height {
            mod_map.rocks[y][x] = !mod_map.rocks[y][x];
            for new_solution in solve_many(&mod_map) {
                if new_solution != initial_solution {
                    return new_solution;
                }
            }
            mod_map.rocks[y][x] = !mod_map.rocks[y][x];
        }
    }
    panic!()
}

fn main() {
    let input = fs::read_to_string("input/day-13.txt").unwrap();
    let maps = input.split("\n\n").map(parse_map).collect_vec();

    println!("Part 1: {}", maps.iter().map(|x| score(solve_one(x))).sum::<usize>());
    println!("Part 2: {}", maps.iter().map(|x| score(unsmudge(x))).sum::<usize>());
}
