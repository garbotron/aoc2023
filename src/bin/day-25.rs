use std::collections::HashSet;
use std::{collections::HashMap, fs};

use itertools::Itertools;
use pathfinding::prelude::*;
use pathfinding::directed::dijkstra::dijkstra_all;

fn farthest_path_from<'a>(start: &'a str, edges: &'a HashMap<&'a str, Vec<&'a str>>) -> Vec<&'a str> {
    let reachables = dijkstra_all(
        &start,
        |n| edges.get(*n).unwrap().iter().map(|s| (*s, 1)));

    reachables.keys()
        .map(|x| build_path(x, &reachables))
        .max_by_key(|x| x.len()).unwrap()
}

fn farthest_path<'a>(edges: &'a HashMap<&'a str, Vec<&'a str>>) -> Vec<&'a str> {
    edges.keys()
        .map(|x| farthest_path_from(x, edges))
        .max_by_key(|x| x.len()).unwrap()
}

fn solve<'a>(start: &'a str, end: &'a str, edges: &'a HashMap<&'a str, Vec<&'a str>>, cuts: &HashSet<(&'a str, &'a str)>, remaining_cuts: i32) -> Option<usize> {
    let solution = dijkstra(
        &start,
        |n| edges.get(*n).unwrap().iter().filter(|s| !cuts.contains(&(n, s))).map(|s| (*s, 1)),
        |n| n.eq(&end));
    if let Some((path, _)) = solution {
        if remaining_cuts == 0 {
            None // we were unable to separate the 2
        } else {
            for (x, y) in path.iter().tuple_windows() {
                let mut new_cuts = cuts.clone();
                new_cuts.insert((x, y));
                new_cuts.insert((y, x));
                if let Some(result) = solve(start, end, edges, &new_cuts, remaining_cuts - 1) {
                    return Some(result);
                }
            }
            None // nowhere we can cut led to a solution
        }
    } else {
        if remaining_cuts == 0 {
            // we're good!
            let group1_size = dijkstra_all(&start, |n| edges.get(*n).unwrap().iter().filter(|s| !cuts.contains(&(n, s))).map(|s| (*s, 1))).len();
            let group2_size = dijkstra_all(&end, |n| edges.get(*n).unwrap().iter().filter(|s| !cuts.contains(&(n, s))).map(|s| (*s, 1))).len();
            Some((group1_size + 1) * (group2_size + 1))
        } else {
            panic!() // we separated the 2 with less than 3 cuts (shoudn't happen)
        }
    }
}

fn main() {
    let input = fs::read_to_string("input/day-25.txt").unwrap();

    let mut edges: HashMap<&str, Vec<&str>> = HashMap::new();
    for line in input.lines() {
        let (lhs, rhs) = line.split_once(": ").unwrap();
        for w in rhs.split_whitespace() {
            edges.entry(lhs).or_default().push(w);
            edges.entry(w).or_default().push(lhs);
        }
    }

    let path = farthest_path(&edges);
    let start = path[0];
    let end = path[path.len() - 1];

    println!("{}", solve(start, end, &edges, &HashSet::new(), 3).unwrap());
}
