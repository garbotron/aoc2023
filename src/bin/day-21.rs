/*

As usual, part 2 throws everything for a loop.
I spent a long time on this one...

The maps are always identical width x height, and there is a blank stripe across the center in both directions.
So eventually, the outward progression of steps will stabilize on a pattern with a period of <map width>.
The only step count we're interested in is the final one.
Let's look for patterns that can be determined mathematically.

Looking at the first example:
F(6) = 16
F(17) = 145
F(28) = 460
F(39) = 944
F(50) = 1594
F(61) = 2406
F(72) = 3380
F(83) = 4516
F(94) = 5814
F(105) = 7274
F(116) = 8896
F(127) = 10680
F(138) = 12626
F(149) = 14734
F(160) = 17004
F(171) = 19436
F(182) = 22030
F(193) = 24786
F(204) = 27704
F(215) = 30784
...
F(5000) = ?

If we plug these values into Wolfram Alpha, it shows that the 3rd differences level off to 0 after the 4th term. Excluding the first 3 terms, we get a closed form:
a_n = 81 n^2 + 407 n + 456
(where n=1 is 39, n=2 is 50, etc)

Rephrasing this in terms of the function we want:
F(n) = a_((n - 28) / 11)
     = 81 ((n - 28) / 11)^2 + 407 ((n - 28) / 11) + 456
     = (81 n^2 - 59 n - 6676) / 121

F(5000) = 16733044
Perfect.

Moving on to the real input...
F(65) = 3778
F(196) = 33695
F(327) = 93438
F(458) = 183007
F(589) = 302402
F(720) = 451623
F(851) = 630670
F(982) = 839543
F(1113) = 1078242
F(1244) = 1346767
F(1375) = 1645118
F(1506) = 1973295
F(1637) = 2331298
F(1768) = 2719127
F(1899) = 3136782
F(2030) = 3584263
F(2161) = 4061570
F(2292) = 4568703
F(2423) = 5105662
F(2554) = 5672447
...
F(26501365) = ?

For this case we don't even need to exclude anything from the start. We get a closed form right away.
a_n = 14913 n^2 - 14822 n + 3687

F(n) = a_((n + 66) / 131)
     = 14913 ((n + 66) / 131)^2 - 14822 ((n + 66) / 131) + 3687
     = (14913 n^2 + 26834 n + 82623) / 17161

F(26501365) = 610321885082978

*/

use std::{fs, collections::HashSet};
use itertools::Itertools;

#[derive(Debug)]
struct Map {
    width: i64,
    height: i64,
    barriers: HashSet<(i64, i64)>,
}

#[derive(Clone, Debug)]
struct State {
    step_count: i64,
    unsolved: HashSet<(i64, i64)>,
    even_solved: HashSet<(i64, i64)>,
    odd_solved: HashSet<(i64, i64)>,
}

impl Map {
    fn barrier_at(&self, (x, y): (i64, i64)) -> bool {
        let barrier_x = x.rem_euclid(self.width);
        let barrier_y = y.rem_euclid(self.height);
        self.barriers.contains(&(barrier_x, barrier_y))
    }
}

impl State {
    fn count(&self) -> i64 {
        ((if self.step_count % 2 == 0 { self.even_solved.len() } else { self.odd_solved.len() }) + self.unsolved.len()) as i64
    }
}

fn parse_input(s: &str) -> (Map, State) {
    let mut start = HashSet::new();
    let mut barriers = HashSet::new();
    for (y, l) in s.lines().enumerate() {
        for (x, c) in l.chars().enumerate() {
            match c {
                'S' => { start.insert((x as i64, y as i64)); }
                '#' => { barriers.insert((x as i64, y as i64)); }
                _ => { },
            }
        }
    }
    let width = s.lines().next().unwrap().len() as i64;
    let height = s.lines().count() as i64;
    (Map { width, height, barriers }, State { step_count: 0, unsolved: start, even_solved: HashSet::new(), odd_solved: HashSet::new() })
}

fn iterate_state(state: &mut State, map: &Map) {
    let (my_solved, other_solved) = if state.step_count % 2 == 0 {
        (&mut state.even_solved, &mut state.odd_solved)
    } else {
        (&mut state.odd_solved, &mut state.even_solved)
    };
    for (x, y) in state.unsolved.drain().collect_vec() {
        my_solved.insert((x, y));
        for n in [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)] {
            if !map.barrier_at(n) && !other_solved.contains(&n) {
                state.unsolved.insert(n);
            }
        }
    }
    state.step_count += 1;
}

fn solve_brute(iters: i64, initial_state: &State, map: &Map) -> i64 {
    let mut state = initial_state.clone();
    for _ in 0..iters {
        iterate_state(&mut state, &map);
    }
    state.count() as i64
}

fn main() {
    let input = fs::read_to_string("input/day-21.txt").unwrap();
    let (map, initial_state) = parse_input(&input);

    println!("Part 1: {}", solve_brute(64, &initial_state, &map));

    let mut to_check = Vec::new();
    let mut i = 26501365;
    while i > 0 {
        to_check.push(i);
        i -= 131;
    }
    to_check.reverse();
    for &x in to_check.iter().take(20) {
        println!("F({x}) = {}", solve_brute(x, &initial_state, &map));
    }

    // See math above.
    println!("Part 2: 610321885082978");
}
