/*
Part 1 can be modelled as a simplified version of part 2.
The problem can be expressed like this:

We start with a set of ID ranges, which we will call a blob (A..B + C..D + ...).
For each map in the input, we're going to take any matching sub-ranges and add (or subtract) an offset.
This means splitting the current blob into sub-blobs where there are intersections with the map ranges.
We repeat this process for each map, then take the lowest min value of the blob.
*/

use std::fs;
use itertools::Itertools;

type Range = std::ops::Range<i64>;
type Blob = Vec<Range>;
struct RangeWithOffset { range: Range, offset: i64 }
struct Map { ranges: Vec<RangeWithOffset> }

fn parse_map(s: &str) -> Map {
    let mut ret = Map { ranges: Vec::new() };
    for line in s.lines().skip(1) {
        let nums = line.split_whitespace().map(|s| s.parse::<i64>().unwrap()).collect_vec();
        let (dst_start, src_start, len) = (nums[0], nums[1], nums[2]);
        ret.ranges.push(RangeWithOffset { range: src_start..(src_start + len), offset: dst_start - src_start });
    }
    ret
}

fn apply_map(blob: &Blob, map: &Map) -> Blob {
    let mut src_blob = blob.clone();
    let mut dst_blob = Blob::new();

    while let Some(range) = src_blob.pop() {
        if let Some(map_range) = map.ranges.iter().find(|r| r.range.contains(&range.start) || r.range.contains(&(range.end - 1))) {
            let before_map_range = range.start..map_range.range.start;
            if !before_map_range.is_empty() {
                src_blob.push(before_map_range);
            }
            let after_map_range = map_range.range.end..range.end;
            if !after_map_range.is_empty() {
                src_blob.push(after_map_range);
            }
            let in_map_range = range.start.max(map_range.range.start)..range.end.min(map_range.range.end);
            if !in_map_range.is_empty() {
                dst_blob.push((in_map_range.start + map_range.offset)..(in_map_range.end + map_range.offset));
            }
        } else {
            dst_blob.push(range); // no overlap
        }
    }

    dst_blob
}

fn apply_maps(blob: &Blob, maps: &Vec<Map>) -> Blob {
    maps.iter().fold(blob.clone(), |blob, map| apply_map(&blob, map))
}

fn main() {
    let input = fs::read_to_string("input/day-05.txt").unwrap();
    let input_chunks = input.split("\n\n").collect_vec();
    let (_, seeds_str) = input_chunks[0].trim().split_once(':').unwrap();
    let seeds = seeds_str.split_whitespace().map(|x| x.parse::<i64>().unwrap()).collect_vec();
    let maps = input_chunks.iter().skip(1).map(|x| parse_map(x)).collect_vec();

    let p1_init_blob = seeds.iter().map(|s| *s..(*s+1)).collect_vec();
    let p1_result = apply_maps(&p1_init_blob, &maps);
    println!("Part 1: {}", p1_result.iter().map(|r| r.start).min().unwrap());

    let p2_init_blob = (0..(seeds.len() / 2)).map(|i| seeds[i * 2]..(seeds[i * 2] + seeds[i * 2 + 1])).collect_vec();
    let p2_result = apply_maps(&p2_init_blob, &maps);
    println!("Part 2: {}", p2_result.iter().map(|r| r.start).min().unwrap());
}
