use std::{fs, collections::HashSet};
use itertools::Itertools;

struct Card {
    winning_nums: HashSet<i32>,
    my_nums: HashSet<i32>,
}

impl Card {
    fn parse(s: &str) -> Self {
        let (_, after_colon) = s.split_once(':').unwrap();
        let (win_s, my_s) = after_colon.split_once('|').unwrap();
        Card {
            winning_nums: win_s.split_whitespace().map(|x| x.parse().unwrap()).collect(),
            my_nums: my_s.split_whitespace().map(|x| x.parse().unwrap()).collect(),
        }
    }

    fn matching_nums_count(&self) -> usize {
        self.winning_nums.intersection(&self.my_nums).count()
    }

    fn points(&self) -> i32 {
        (0..self.matching_nums_count()).fold(0, |acc, _| if acc == 0 { 1 } else { acc * 2 })
    }
}

// There are obviously better ways to do this but this takes less than a minute, so *shrug*
fn get_part2_total(cards: &Vec<Card>) -> usize {
    let mut sum = 0;
    let mut unscored = (0..cards.len()).collect_vec();
    while let Some(card_idx) = unscored.pop() {
        sum += 1;
        for offset in 1..=cards[card_idx].matching_nums_count() {
            unscored.push(card_idx + offset);
        }
    }
    sum
}

fn main() {
    let input = fs::read_to_string("input/day-04.txt").unwrap();
    let cards = input.lines().map(Card::parse).collect_vec();

    println!("Part 1: {}", cards.iter().map(|x| x.points()).sum::<i32>());
    println!("Part 2: {}", get_part2_total(&cards));
}
