use std::{fs, collections::HashMap};
use itertools::Itertools;

#[derive(PartialEq, Eq, PartialOrd, Ord)]
enum HandType {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

fn parse_hand_with_bid(s: &str) -> (Vec<char>, i64) {
    let (a, b) = s.split_once(' ').unwrap();
    (a.chars().collect(), b.parse().unwrap())
}

fn card_strength(card: &char) -> i64 {
    match card {
        '*' => 1, // Joker
        'A' => 14,
        'K' => 13,
        'Q' => 12,
        'J' => 11,
        'T' => 10,
        x => x.to_string().parse().unwrap(),
    }
}

fn hand_type(hand: &Vec<char>) -> HandType {
    let mut card_counts = HashMap::new();
    for card in hand.iter() {
        let count = card_counts.get(card).unwrap_or(&0);
        card_counts.insert(*card, *count + 1);
    }

    let joker_count = *card_counts.get(&'*').unwrap_or(&0);
    card_counts.remove(&'*');

    let match5 = card_counts.values().filter(|x| **x == 5).count();
    let match4 = card_counts.values().filter(|x| **x == 4).count();
    let match3 = card_counts.values().filter(|x| **x == 3).count();
    let match2 = card_counts.values().filter(|x| **x == 2).count();

    match joker_count {
        5 => HandType::FiveOfAKind,
        4 => HandType::FiveOfAKind,
        3 if match2 == 1 => HandType::FiveOfAKind,
        3 => HandType::FourOfAKind,
        2 if match3 == 1 => HandType::FiveOfAKind,
        2 if match2 == 1 => HandType::FourOfAKind,
        2 => HandType::ThreeOfAKind,
        1 if match4 == 1 => HandType::FiveOfAKind,
        1 if match3 == 1 => HandType::FourOfAKind,
        1 if match2 == 2 => HandType::FullHouse,
        1 if match2 == 1 => HandType::ThreeOfAKind,
        1 => HandType::OnePair,
        0 if match5 == 1 => HandType::FiveOfAKind,
        0 if match4 == 1 => HandType::FourOfAKind,
        0 if match3 == 1 && match2 == 1 => HandType::FullHouse,
        0 if match3 == 1 => HandType::ThreeOfAKind,
        0 if match2 == 2 => HandType::TwoPair,
        0 if match2 == 1 => HandType::OnePair,
        _ => HandType::HighCard,
    }
}

fn total_winnings(hands_with_bids: &Vec<(Vec<char>, i64)>) -> i64 {
    hands_with_bids.iter()
        .sorted_by_key(|(x, _)| (hand_type(x), x.iter().map(card_strength).collect_vec()))
        .enumerate()
        .map(|(i, (_, bid))| (i + 1) as i64 * bid).sum::<i64>()
}

fn main() {
    let input = fs::read_to_string("input/day-07.txt").unwrap();
    let hands_with_bids = input.lines().map(parse_hand_with_bid).collect_vec();

    println!("Part 1: {}", total_winnings(&hands_with_bids));

    let mut hands_with_bids_p2 = hands_with_bids.clone();
    for (h, _) in hands_with_bids_p2.iter_mut() {
        for c in h.iter_mut() {
            if *c == 'J' {
                *c = '*';
            }
        }
    }
    println!("Part 2: {}", total_winnings(&hands_with_bids_p2));
}
