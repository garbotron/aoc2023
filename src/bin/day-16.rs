use std::{fs, collections::HashSet};
use itertools::Itertools;

#[derive(PartialEq, Eq, Hash, Clone, Debug)]
struct Beam { x: i32, y: i32, dx: i32, dy: i32 }

fn beam_step(b: &Beam, map: &Vec<Vec<char>>) -> Vec<Beam> {
    let turned = match map[b.y as usize][b.x as usize] {
        '/' => vec![(-b.dy, -b.dx)],
        '\\' => vec![(b.dy, b.dx)],
        '-' if b.dy != 0 => vec![(-1, 0), (1, 0)],
        '|' if b.dx != 0 => vec![(0, -1), (0, 1)],
        _ => vec![(b.dx, b.dy)],
    };
    turned.iter().filter_map(|&(dx, dy)| {
        let (x, y) = (b.x + dx, b.y + dy);
        if y < 0 || y as usize >= map.len() || x < 0 || x as usize >= map[y as usize].len() {
            None
        } else {
            Some(Beam { x, y, dx, dy })
        }
    }).collect()
}

fn energized_count(map: &Vec<Vec<char>>, (x, y, dx, dy): (i32, i32, i32, i32)) -> usize {
    let mut outstanding_beams = vec![Beam { x, y, dx, dy }];
    let mut seen_beams = HashSet::new();
    while let Some(b) = outstanding_beams.pop() {
        if seen_beams.insert(b.clone()) {
            for next in beam_step(&b, &map) {
                outstanding_beams.push(next);
            }
        }
    }
    seen_beams.iter().map(|b| (b.x, b.y)).unique().count()
}

fn main() {
    let input = fs::read_to_string("input/day-16.txt").unwrap();
    let map = input.lines().map(|x| x.chars().collect_vec()).collect_vec();

    println!("Part 1: {}", energized_count(&map, (0, 0, 1, 0)));

    let start_top = (0..map[0].len() as i32).map(|x| (x, 0, 0, 1)).collect_vec();
    let start_bottom = (0..map[0].len() as i32).map(|x| (x, map.len() as i32 - 1, 0, -1)).collect_vec();
    let start_left = (0..map.len() as i32).map(|y| (0, y, 1, 0)).collect_vec();
    let start_right = (0..map.len() as i32).map(|y| (map[0].len() as i32 - 1, y, 1, 0)).collect_vec();
    let start_all = [start_top, start_bottom, start_left, start_right].concat();
    println!("Part 2: {}", start_all.iter().map(|x| energized_count(&map, *x)).max().unwrap());
}
