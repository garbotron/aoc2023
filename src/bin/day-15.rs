use std::fs;
use itertools::Itertools;

enum Step {
    Remove(usize, String),
    Insert(usize, String, usize),
}

fn parse_step(s: &str) -> Step {
    if let Some((a, _)) = s.split_once('-') {
        Step::Remove(hash(a), a.to_string())
    } else if let Some((a, b)) = s.split_once('=') {
        Step::Insert(hash(a), a.to_string(), b.parse().unwrap())
    } else {
        panic!()
    }
}

fn hash(s: &str) -> usize {
    s.chars().fold(0, |acc, c| ((acc + c as usize) * 17) % 256)
}

fn main() {
    let input = fs::read_to_string("input/day-15.txt").unwrap();
    let steps = input.trim().split(',').collect_vec();

    println!("Part 1: {}", steps.iter().map(|s| hash(s)).sum::<usize>());

    let steps = steps.iter().map(|s| parse_step(s)).collect_vec();
    let mut boxes: Vec<Vec<(String, usize)>> = vec![vec![]; 256];
    for step in steps {
        match step {
            Step::Remove(hash, label) => boxes[hash].retain(|(x, _)| !label.eq(x)),
            Step::Insert(hash, label, focal_len) => {
                if let Some((_, lens)) = boxes[hash].iter_mut().find(|(x, _)| label.eq(x)) {
                    *lens = focal_len;
                } else {
                    boxes[hash].push((label, focal_len));
                }
            },
        }
    }

    let mut focusing_power = 0;
    for (i, b) in boxes.iter().enumerate() {
        for (j, (_, focal_len)) in b.iter().enumerate() {
            focusing_power += (i + 1) * (j + 1) * *focal_len;
        }
    }
    println!("Part 2: {focusing_power}");
}
