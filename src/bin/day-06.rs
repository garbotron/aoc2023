use std::fs;
use itertools::Itertools;

fn ways_to_beat(distance_to_beat: i64, time: i64) -> i64 {
    let mut ret = 0;
    for hold_time in 1..time {
        let distance = (time - hold_time) * hold_time;
        if distance > distance_to_beat {
            ret += 1;
        }
    }
    ret
}

fn main() {
    let input = fs::read_to_string("input/day-06.txt").unwrap();
    let lines = input.lines().collect_vec();

    let p1_times = lines[0].split_whitespace().skip(1).map(|x| x.parse::<i64>().unwrap()).collect_vec();
    let p1_distances = lines[1].split_whitespace().skip(1).map(|x| x.parse::<i64>().unwrap()).collect_vec();
    let mut p1_result = 1;
    for race_idx in 0..p1_times.len() {
        let time = p1_times[race_idx];
        let distance_to_beat = p1_distances[race_idx];
        p1_result *= ways_to_beat(distance_to_beat, time);
    }
    println!("Part 1: {}", p1_result);

    let p2_time = lines[0].replace(" ", "").split(':').skip(1).next().unwrap().parse::<i64>().unwrap();
    let p2_distance = lines[1].replace(" ", "").split(':').skip(1).next().unwrap().parse::<i64>().unwrap();
    println!("Part 2: {}", ways_to_beat(p2_distance, p2_time));
}
