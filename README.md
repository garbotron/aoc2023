# aoc2023

[Advent of Code](https://adventofcode.com/) solutions, written in whatever language I feel like. Will Rust work for AoC? Probably! It did last year.
